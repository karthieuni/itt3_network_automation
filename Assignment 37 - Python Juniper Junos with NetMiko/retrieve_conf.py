#!/usr/bin/env python3
from netmiko import ConnectHandler

# Attributes for establishing connection to target devices
vSRX_1 = {
    'device_type': 'juniper',
    'ip': '10.10.1.1',
    'username': 'root',
    'password': 'lab123',
}

vSRX_2 = {
    'device_type': 'juniper',
    'ip': '10.10.2.1',
    'username': 'root',
    'password': 'lab123',
}

def main():
    try:
        # Delay_factor is used as a multiplication factor for timing delays
        net_connect = ConnectHandler(**vSRX_1, global_delay_factor=1)
        net_connect_1 = ConnectHandler(**vSRX_2, global_delay_factor=1)

        output = net_connect.send_command('show configuration')
        output_1 = net_connect_1.send_command('show configuration')

        # Stores both SRX configurations as a file
        outFile = open('vSRX1.conf','w')
        outFile.write(output)
        outFile.close()

        outFile2 = open('vSRX2.conf','w')
        outFile2.write(output_1)
        outFile2.close()
        
    # Catch and print any exceptions
    except Exception as a:
        print(a)
main()