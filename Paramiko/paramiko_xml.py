import paramiko, time, os

try
        sshClient = paramiko.SSHClient()
        sshClient.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print 'Trying to connect to 10.217.19.160'
        sshClient.connect('10.217.19.160', username='root', password='lab123')

        channel = sshClient.invoke_shell()
        print 'SRX shell invoked'
        time.sleep(2)

        os.system('clear')

        channel.send('clin')
        time.sleep(1)

        routerOutput = channel.recv(1000) # Read router replies

        channel.send('show configuration  display xml  no-moren')
        time.sleep(0.7)

        output = channel.recv(15000)
        print output
        channel.close()

        outFile = open('SRXconf.cfg','w')
        outFile.write(output)
        outFile.close()

except Exception as ex
        print 'Something went wrong.'
        print ex
