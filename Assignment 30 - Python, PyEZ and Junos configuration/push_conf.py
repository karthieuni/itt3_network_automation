from jnpr.junos import Device
from lxml import etree
from jnpr.junos.utils.config import Config
from sys import argv

HOSTIP = '10.217.19.162'
Username = 'root'
Pass = 'lab123'

user_input = input("Hostname: ")

#Saves config to a XML-file
def config_file(config):
    with open("config.xml","w") as handler:
        handler.write(config)

#The active configuration is currently operational on the system (already in the system/rollback 0):
def main(hostname=user_input):
    with Device(host = HOSTIP, user = Username, password = Pass) as dev:
         cu = Config(dev)

         #Get config from router and save to file
         config=etree.tostring(dev.rpc.get_config()).decode()
         config_file(config)

         #Load config into XML element
         tree=etree.parse("config.xml")
         root=tree.getroot()

         #Find host-name entry in config
         for elem in root:
             name=elem.find("host-name")
             if name is not None:
                 print("Current host-name:\t", name.text)
                 #Replace interface
                 name.text=hostname

         #Save changed config to file
         config = etree.tostring(tree).decode()
         config_file(config)

         #Upload config to juniper device candidate config
         cu.load(etree.tostring(tree).decode(), merge=True)

         #Commit changes
         if cu.commit_check():
             cu.commit()
             print("New host-name:\t\t", hostname)

main()
