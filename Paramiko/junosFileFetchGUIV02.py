# Python 2.7 on Linux
import Tkinter
import ScrolledText as tkst # Scrolled text widget for large text display
import paramiko, time, os

class FileFetchGUI():
    def __init__(self, sshComm):
        self.sshCommunication = sshComm

        self.master = Tkinter.Tk(  )            # Create master widget object
        self.junosIPVar = Tkinter.StringVar()   # Prepare a variable to hold

        self.toggle = 0

        self.junosIPVar     = '10.217.19.160'
        self.junosUser      = 'root'
        self.junosPasswrd   = 'lab123'
        self.junosPath      = '//'
        self.junosFileName  = '???'
        self.destIP         = '10.217.19.164'
        self.dstUserName    = 'osboxes'
        self.dstPasswrd     = 'oxboxes.org'
        self.dstPath        = '/home/osboxes'
        self.dstFileName    = 'junos.conf.zg'

        Tkinter.Label(self.master, text="Junos Network device: ").grid(row=0, sticky="E")
        Tkinter.Label(self.master, text="       ").grid(row=0, column=4, sticky="E") # Spa
        Tkinter.Label(self.master, text="IP:").grid(row=1, sticky="W")
        Tkinter.Label(self.master, text="User name:").grid(row=2, sticky="W")
        Tkinter.Label(self.master, text="Password:").grid(row=3, sticky="W")
        Tkinter.Label(self.master, text="File path:").grid(row=4, sticky="W")
        Tkinter.Label(self.master, text="File name:").grid(row=5, sticky="W")
        Tkinter.Label(self.master, text="Receiving (this) Network device: ").grid(row=6, sticky="E")
        Tkinter.Label(self.master, text="IP:").grid(row=7, sticky="W")
        Tkinter.Label(self.master, text="User name:").grid(row=8, sticky="W")
        Tkinter.Label(self.master, text="Password:").grid(row=9, sticky="W")
        Tkinter.Label(self.master, text="File path:").grid(row=10, sticky="W")
        Tkinter.Label(self.master, text="File name:").grid(row=11, sticky="W")
        Tkinter.Label(self.master, text="").grid(row=12, sticky="W") # Spacer label
        Tkinter.Label(self.master, text="").grid(row=14, sticky="W") # Spacer label
        # Create entry boxes
        self.entry1 = Tkinter.Entry(self.master)
        self.entry2 = Tkinter.Entry(self.master)
        self.entry3 = Tkinter.Entry(self.master)
        self.entry4 = Tkinter.Entry(self.master)
        self.entry5 = Tkinter.Entry(self.master)
        self.entry6  = Tkinter.Entry(self.master)
        self.entry7  = Tkinter.Entry(self.master)
        self.entry8  = Tkinter.Entry(self.master)
        self.entry9  = Tkinter.Entry(self.master)
        self.entry10 = Tkinter.Entry(self.master)
        # Place entry boxes in the grid
        self.entry1.grid(row=1, column=1)
        self.entry2.grid(row=2, column=1)
        self.entry3.grid(row=3, column=1)
        self.entry4.grid(row=4, column=1)
        self.entry5.grid(row=5, column=1)
        self.entry6.grid(row=7, column=1)
        self.entry7.grid(row=8, column=1)
        self.entry8.grid(row=9, column=1)
        self.entry9.grid(row=10, column=1)
        self.entry10.grid(row=11, column=1)
        # Insert a default text in entry boxes

        self.entry1.insert(0, self.junosIPVar)
        self.entry2.insert(0, self.junosUser)
        self.entry3.insert(0, self.junosPasswrd)
        self.entry4.insert(0, self.junosPath)
        self.entry5.insert(0, self.junosFileName)
        self.entry6.insert(0, self.destIP)
        self.entry7.insert(0, self.dstUserName)
        self.entry8.insert(0, self.dstPasswrd)
        self.entry9.insert(0, self.dstPath)
        self.entry10.insert(0, self.dstFileName)

        # http://effbot.org/Tkinterbook/entry.htm
        # Create buttons
        self.listEntriesButton = Tkinter.Button(self.master,
                                text = "List entries",
                                command = self.listEntriesButtonAction)

        self.clearEntriesButton = Tkinter.Button(self.master,
                                text = "Clear entries",
                                command = self.clearEntriesButtonAction)

        self.fetchFileButton    = Tkinter.Button(self.master,
                                text = "Fetch file",
                                command = self.fetchFileButtonAction)

        self.getIPButton    = Tkinter.Button(self.master,
                                text = "Get IP",
                                command = self.getIPButtonAction)

        self.quitButton         = Tkinter.Button(self.master,
                                text='Quit',
                                command=self.master.destroy)
        # Place buttons in the grid
        self.quitButton.grid(row=15, column=2)
        self.quitButton.config(relief="sunken")
        self.fetchFileButton.grid(row=1, column=2)
        self.fetchFileButton.config(relief="sunken")
        self.getIPButton.grid(row=7,column=2)
        self.listEntriesButton.grid(row=13, column=0)
        self.clearEntriesButton.grid(row=14, column=0)
        # Create info scrollable output text field
        self.infoTextField = tkst.ScrolledText(self.master, undo=True,
                                  # wrap text at full words only
                                    wrap   = 'word',
                                    width  = 60,      # characters
                                    height = 10,      # text lines
                                    # background color of edit area
                                    bg='grey' )

        self.infoTextField.grid(row=13, column=1, columnspan=2, sticky="W")

        self.master.mainloop()

    def listEntriesButtonAction(self):
        junoIP          = str(self.entry1.get())
        junoUser        = str(self.entry2.get())
        junoPasswrd     = str(self.entry3.get())
        junoPath        = str(self.entry4.get())
        junoFileName    = str(self.entry5.get())
        destIP          = str(self.entry6.get())
        destUserName    = str(self.entry7.get())
        destPasswrd     = str(self.entry8.get())
        destPath        = str(self.entry9.get())
        destFileName    = str(self.entry10.get())

        self.infoTextField.insert('insert', junoIP + "\n" + junoUser + "\n" +
                                  junoPasswrd + "\n" + junoFileName + "\n" +
                                  destIP + "\n" + destUserName + "\n" +
                                  destIP + "\n" + destPasswrd + "\n" +
                                  destPath + "\n" + destFileName + "\n")

    def clearEntriesButtonAction(self):
            junosIP = str(self.entry1.get())
            self.infoTextField.delete('0.0', 'end')
            print(junosIP)

    def getIPButtonAction(self):
        junosIP = str(self.entry6.get())
        self.infoTextField.insert('insert', junosIP + "\n")
        print(junosIP)


    def fetchFileButtonAction(self):
        junoIP          = str(self.entry1.get())
        junoUser        = str(self.entry2.get())
        junoPasswrd     = str(self.entry3.get())
        junoPath        = str(self.entry4.get())
        junoFileName    = str(self.entry5.get())
        destIP          = str(self.entry6.get())
        destUserName    = str(self.entry7.get())
        destPasswrd     = str(self.entry8.get())
        destPath        = str(self.entry9.get())
        destFileName    = str(self.entry10.get())
        self.sshCommunication.fetchConfiguration(self, junoIP, junoUser, junoPasswrd, junoPath,
                                   destPath, destUserName, destIP, destPasswrd)
        if self.toggle == 0:
            self.entry1.delete(0, 'end') # Delete text from entry field
            self.toggle = 1
        else:
            self.entry1.delete(0, 'end') # Delete text from entry field
            self.entry1.insert(0, 'Button pressed')
            self.toggle = 0

class SshCommunication():

    def __init__(self):
        print('sshCommunication object created')


    def fetchConfiguration(self, GUI, junosIP, userName, password, junosConfigPath,
                           hostConfigPath, hostUserName, hostIP, hostPassword):
        try:
            GUI.infoTextField.insert('insert', "Please wait. I am working on it." + "\n")
            self.sshClient = paramiko.SSHClient()
            self.sshClient.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            print('Trying to connect to 10.217.19.160')
            self.sshClient.connect('10.217.19.160', username='root', password='lab123')

            self.channel = self.sshClient.invoke_shell()
            print('SRX shell invoked')
            time.sleep(1)

            os.system('clear')

            routerOutput = self.channel.recv(1000)  # Read router replies - empty buffer

            self.config = '/config/juniper.conf.gz'
            self.configCopy = '//home/osboxes/juniper4.conf.gz'

            self.channel.send('scp ' + self.config + ' osboxes@10.217.19.164:' + self.configCopy + '\n')
            time.sleep(1)

            self.channel.send('osboxes.org\n')
            time.sleep(1)

            output = self.channel.recv(1000)
            print(output)
            self.channel.close()
            GUI.infoTextField.insert('insert', "Hi from SshCommunication" + "\n")
            GUI.infoTextField.insert('insert', output )

        except Exception as  ex:
            print('Something went wrong.')
            print(ex)

class JunosConfig():

    def __init__(self):
        try:
            self.sshCommunication = SshCommunication()
            FileFetchGUI(self.sshCommunication)
        except Exception as  ex:
            print('Something went wrong:')
            print(ex)

def main():
    JunosConfig()

main()