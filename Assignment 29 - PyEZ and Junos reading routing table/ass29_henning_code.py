  #########################
  #### Made by Henning ####
  #########################

from jnpr.junos import Device
from pprint import pprint
from lxml import etree
from time import sleep
import subprocess, platform
import ipaddress, sys, os
import xmltodict
#from queue import Queue

#Specify the size of the python command line window.
os.system("mode con cols=54 lines=65")

dev = " "
devip = "192.168.10.1"
user = "root"
password = "qaz123"
c = 0

def main():

	#vSRX-Info gets the information by the use of dev.rpc.get.
	print("\n")
	print(" >>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<< ")
	print(" >>>>      %%% Get vSRX information  %%%       <<<< ")
	print(" >>>>   Program can get diffrent infomation    <<<< ")
	print(" >>>>   From a vSRX and show it in terminal    <<<< ")
	print(" >>>>   User choose what to display 1 2 3..    <<<< ")
	print(" >>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<< ")

	userinput()

def connect():

	#Input a IP address and connect to the router.
	print("\n")
	print(" >>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<< ")
	print(" >>>>        %%% Connect to Router  %%%        <<<< ")
	print(" >>>>    Connect to any juniper router and     <<<< ")
	print(" >>>>   get the information out it you need    <<<< ")
	print(" >>>>     Defualt User/Password can be set     <<<< ")
	print(" >>>>          under [8] in main menu          <<<< ")
	print(" >>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<< ")

	global devip
	global dev

	print("\nDefualt settings: \n                  IP:       ",devip,"\n                  USER:     ",user,"\n                  PASSWORD: ",password)

	defualt = input("\nSet the IP adresse of the Juniper router (Press any key for defualt):\n >>>> ")

	if defualt == "":
		devip = devip

	elif defualt != devip:
		devip = defualt

	try:
		dev = Device(host=devip, user=user, password=password)
		print("\nConnecting to...",devip," with user: ",user," and password:",password," \n")
		dev.open()
		print("\n                >>>> Connected <<<< ")

	except Exception as error:
		print("\nThere is a problem connecting to",devip,"Please reenter the router IP adresse:\n\n")
		print(error, devip)
		global c
		c +=1
		if c == 3:
			print("\nSomthing seams to be wrong, try to change the user or password")
			setdevice()
		else:
			connect()

def userinput():

	#Gets the input choose from the user.
	try:
		UserInput = int(input("\n >>>> What information do you want to see >>>>\n >>>> [1]: Route\n >>>> [2]: Interface\n >>>> [3]: ARP table\n >>>> [4]: OSPF Neighbor\n >>>> [5]: Security Zones\n >>>> [8]: Set the Device\n >>>> [9]: Exit \n\n >>>> "))
		print(" >>>> You entered to >>>", UserInput, "<<< \n")
		os.system("clear")

	except ValueError:
		print("\n >>>> Wrong Input !!! Use numbers :-) .1.2.3 etc. <<<< ")
		sleep(0.5)
		main()

	show(UserInput)

def show(UserInput):

	#Show the requested information from the router.
	if UserInput == 1:
		print("\nRoute Table Information:")
		print (etree.tostring(dev.rpc.get_route_information({"format":"text"},terse=True)).decode().replace("&gt;",">").replace("<output>"," ").replace("</output>"," "),"\n")
		sleep(1)
		print("Dictionary Resualt -> shows next hops obtained from the dictionary:")
		dictionary()
		sleep(1)
		main()

	elif UserInput == 2:
		print("\nInterface Information\n")
		print (etree.tostring(dev.rpc.get_interface_information({"format":"text"},terse=True)).decode().replace("&gt;",">").replace("<output>"," ").replace("</output>"," "),"\n")
		sleep(1)
		main()

	elif UserInput == 3:
		print("\nARP Table Information\n")
		print (etree.tostring(dev.rpc.get_arp_table_information({"format":"text"})).decode().replace("&gt;",">").replace("<output>"," ").replace("</output>"," "),"\n")
		sleep(1)
		main()

	elif UserInput == 4:
		print("\nShow OSPF Neighbor Information\n")
		print (etree.tostring(dev.rpc.get_ospf_neighbor_information({"format":"text"})).decode().replace("&gt;",">").replace("<output>"," ").replace("</output>"," "),"\n")
		sleep(1)
		main()

	elif UserInput == 5:
		print("\nShow Security Zones\n")
		print (etree.tostring(dev.rpc.get_zones_information({"format":"text"})).decode().replace("&gt;",">").replace("<output>"," ").replace("</output>"," "),"\n")
		sleep(1)
		main()

	elif UserInput == 8:

		print("\nSetting up the device")
		sleep(0.5)
		setdevice()

	elif UserInput == 9:
		print("\nExit\n")
		print("\nLeaving the program...\n")
		sleep(0.5)
	else:
		print("\nError!!!... ",UserInput," is not a legal input, try again")
		print(type(UserInput))
		main()

def dictionary():

	#Convert route table to dictionary
	dxml = etree.tostring(dev.rpc.get_route_information(terse=True)).decode()
	xmld = xmltodict.parse(dxml)

	#Print routes from dictionary
	for dest in xmld["route-information"]["route-table"]["rt"]:
		route = dest["rt-destination"] + "       \t--->\t"
		if "nh" in dest["rt-entry"].keys():
			if "to" in dest["rt-entry"]["nh"].keys():
				route += dest["rt-entry"]["nh"]["to"]
			else:
				route += dest["rt-entry"]["nh"]["via"]
		else:
			route += dest["rt-entry"]["nh-type"]
	print(route)

def setdevice():

	#vSRX-Info gets the information by the use of dev.rpc.get.
	print("\n")
	print(" >>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<< ")
	print(" >>>>[8]    %%% Change the settings  %%%       <<<< ")
	print(" >>>>   Here the settings for the connection   <<<< ")
	print(" >>>>   can be changed, both IP address and    <<<< ")
	print(" >>>>             User/Password.               <<<< ")
	print(" >>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<< ")

	global devip
	global user
	global password

	print("\nHere the settings being used can be seen:")
	print("\nThe IP address: ",devip)
	print("The User: ",user)
	print("The Password: ",password)

	try:
		SetUserInput = input("\n >>>> What information do you want to change >>>>\n >>>> [D]: Device IP\n >>>> [U]: User\n >>>> [P]: Password\n >>>> [C]: Connect\n >>>> [T]: Test \n >>>> [ANY]: Go back \n\n >>>> ")
		print("\n >>>> You entered >>>> ", SetUserInput, " <<<<")

	except ValueError:
		input("\n >>>> Wrong Input !!!...\n >>>> Press The Key Enter And Try Again ... \n")
		setdevice()

	if SetUserInput == 'D' or SetUserInput == 'd':
		print("\nChange the IP address to connect to:\n")
		devip = input(" >>>> ")
		setdevice()

	elif SetUserInput == 'U' or SetUserInput == 'u':
		print("\nChange the User to connect with:\n")
		user = input(" >>>> ")
		sleep(1)
		setdevice()

	elif SetUserInput == 'P' or SetUserInput == 'p':
		print("\nChange the Password to connect with:\n")
		password = input(" >>>> ")
		sleep(1)
		setdevice()

	elif SetUserInput == 'C' or SetUserInput == 'c':
		print("\nTry to connect to router\n")
		connect()
		sleep(1)
		setdevice()

	elif SetUserInput == 'T' or SetUserInput == 't':
		print("\nTesting the connection with ping\n")
		pingtool(devip)
		sleep(1)
		setdevice()

	else:
		print("\nGoing back to main menu")
		main()

def pingtool(host):

	if host == "":
		print(" >>>> The device IP address must be other than nothing! change it before test:")
	else:
		#Returns True if host (str) responds to a ping request.
		print("Remember that a device may not respond to a ping (ICMP)\nrequest even if the device\host name is valid and correct\n")

		#Ping parameters as function of OS
		ping_str = "-n 1" if  platform.system().lower()=="windows" else "-c 3"
		args = "ping " + " " + ping_str + " " + host
		need_sh = platform.system().lower() != "windows"
		#False if  platform.system().lower()=="windows" else True

		#Ping
		return subprocess.call(args, shell=need_sh) == 0

os.system("clear")
connect()
main()
dev.close()
