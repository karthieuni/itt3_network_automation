What the programs do: 

1. paratest1.py - outputs SRXconf.cfg as a file
2. paramikoSCPV01.py - copies juniper.conf.gz to //home/osboxes/juniper4.conf.gz
3. paramiko_xml.py - outputs as xml
4. paramikoLSV01.py - executes commands on a remote server via SSH connection
5. junosFileFetchGUIV02.py - Displays the programs GUI (Graphical User Interface)

List of useful tips:

* Run programs with just python ...py (python 2.7) pyEZ is NOT needed

The Junos configuration sits in the /config directory on the SRX box:
* root@SRX_XX% cd config
* root@SRX_XX% ls -all

SCP file transfer is tested in a Putty SSH session to be sure it works syntactically:
Command used in %SRX:
* scp /config/juniper.conf.gz osboxes@10.217.19.164://home/osboxes/juniper.conf5.gz

## Install tkinter:
* sudo apt-get install python-tk (restart machine)

https://stackoverflow.com/questions/26702119/installing-tkinter-on-ubuntu-14-04

