from jnpr.junos import Device
from lxml import etree
from jnpr.junos.utils.config import Config
from jnpr.junos.exception import ConnectError
from jnpr.junos.exception import LockError
from jnpr.junos.exception import RpcError
from jnpr.junos.exception import CommitError
from jnpr.junos.exception import UnlockError

HOSTIP = '10.217.19.162'
Username = 'root'
Pass = 'lab123'

user_1 = int(input("1 for active configuration\n2 for candidate configuration\n3 for rollback configuration\n4 for rescue configuration\nMake your choice: "))

#The active configuration is currently operational on the system (already in the system/rollback 0):
def active_conf():
    with Device(host = HOSTIP, user = Username, password = Pass) as dev:
         data = dev.rpc.get_config(options={'database' : 'committed'})
         print (etree.tostring(data, encoding='unicode', pretty_print=True))

#The candidate configuration is a temporary configuration (being edited):
def candidate_conf():
    with Device(host = HOSTIP, user = Username, password = Pass) as dev:
         data = dev.rpc.get_config()
         print (etree.tostring(data, encoding='unicode', pretty_print=True))

def rollback_conf():
    with Device(host = HOSTIP, user = Username, password = Pass) as dev:
         cu = Config(dev)
         diff = cu.diff(rb_id=user_2)
         print (diff)

def rescue_conf():
    with Device(host = HOSTIP, user = Username, password = Pass) as dev:
        cu = Config(dev)
        rescue = cu.rescue(action="get", format="text")
        if rescue is None:
            print ("No existing rescue configuration.")
            print ("Saving rescue configuration.")
            cu.rescue(action="save")
        else:
            print ("Rescue configuration found:")
            print (rescue)

if user_1 == 1:
    active_conf()

elif user_1 == 2:
    candidate_conf()

elif user_1 == 3:
    user_2 = int(input("Rollback number (0-49): "))
    rollback_conf()

elif user_1 == 4:
    rescue_conf()

else:
    print("Incorrect input")

