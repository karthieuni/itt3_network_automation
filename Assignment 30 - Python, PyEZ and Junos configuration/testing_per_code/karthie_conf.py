from jnpr.junos import Device
from lxml import etree
from jnpr.junos.utils.config import Config

HOSTIP = '10.217.19.162'
Username = 'root'
Password = 'lab123'

defaultConfigFile = 'myConfigFile.conf' # Default configuration file name
myConfigPath = '/home/junosConfigurations/'

dev = Device(host = HOSTIP, user = Username , passwd = Password)
print ('Opening connection to ', HOSTIP)
dev.open()
myConfig = dev.rpc.get_config(options={'format':'text'})
myTextConfig = etree.tostring(myConfig, encoding='unicode', \
pretty_print=True)
myTextConfig = '\n'.join(myTextConfig.split('\n')[1:-2])
configFile = input('Please enter config file name in this format (**add a name**.conf): ')
if not configFile: configFile = defaultConfigFile
myConfigFilePath = myConfigPath + configFile
myConfigFile = open(myConfigFilePath, 'w')
configInFile = open(myConfigFilePath, 'r')

def edit_conf():
    myConfigFile.write(myTextConfig) # Write Junos text configuration to file
    myConfigFile.close()

    input('\nPlease EDIT the config file located in: ' + myConfigFilePath + \
          '\nPress ENTER when done:') # Halt program for user to edit configuration

def push_conf():
    print('Opening and uploading configuration file: ' + configFile + ' listed here:')
    fileContent = configInFile.read() # Read configuration in
    configInFile.close()
    print(fileContent) # This is the config that will be sent to Junos

def commit_conf():
    dev.bind(cu=Config) # Bind the Config instance to the Device instance.
    dev.cu.lock() # Lock the configuration.
    dev.cu.load(path=myConfigFilePath, overwrite=True) # Load configuration to Junos
    dev.cu.commit() # Commit the configuration on Junos device
    dev.cu.unlock() # Unlock the configuration
    dev.close()
    print('The listed config was successfully sent to the Junos device at ' + HOSTIP)

edit_conf()
push_conf()
commit_conf()