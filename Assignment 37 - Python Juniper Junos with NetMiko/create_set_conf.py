#!/usr/bin/python3
import netmiko

# Attributes for establishing connection to target devices
myConnection = netmiko.ConnectHandler(ip='10.10.1.1',
                                      device_type='juniper',
                                      username='root',
                                      password='lab123')

myConnection_1 = netmiko.ConnectHandler(ip='10.10.2.1',
                                      device_type='juniper',
                                      username='root',
                                      password='lab123')

def main():
    try:
        # Enter into config_mode
        myConnection.config_mode()
        myConnection_1.config_mode()

        # List configuration set commands
        configSetCommands = myConnection.send_command('show | display set')
        configSetCommands_1 = myConnection_1.send_command('show | display set')

        # Specify the name and the path where the set command file has to be saved
        with open("/home/osboxes/Desktop/vSRX-1_Set_Commands.conf", "wb") as f:
            f.write(configSetCommands.encode("UTF-8"))
        with open("/home/osboxes/Desktop/vSRX-2_Set_Commands.conf", "wb") as g:
            g.write(configSetCommands_1.encode("UTF-8"))

        # Exit configuration mode
        myConnection.exit_config_mode()
        myConnection_1.exit_config_mode()

        # Try to gracefully close the SSH connection
        myConnection.disconnect()
        myConnection_1.disconnect()

    # Catch and print any exceptions
    except Exception as a:
        print(a)
main()