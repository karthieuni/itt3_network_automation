import paramiko, time, os
try:
        sshClient = paramiko.SSHClient() # Create SSHClient object
        sshClient.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print ('Trying to connect to 10.217.19.160')
        sshClient.connect('10.217.19.160', username='root', password='lab123')
        channel = sshClient.invoke_shell() # Create a channel object
        print ('SRX shell invoked')
        time.sleep(2)
        os.system('clear') # Clear/Whipe the terminal window
        channel.send('cli\n') # Enter router the Command Line Interface
        time.sleep(1) # Leave time for the router to enter CLI
        routerOutput = channel.recv(1000) # Read router. Output/replies not used
        # time.sleep(3) # Wait to buffer times out and is empty
        # channel.send('edit\n')
        channel.send('show configuration | no-more\n') # Make router list config
        # shell.send('show route\n')
        time.sleep(0.7) # Leave time for the router to list config
        output = channel.recv(10000) # Read router configuration output
        print output # List the configuration in the terminal window
        channel.close()
        outFile = open('SRXconf.cfg','w')
        outFile.write(output) # Save the configuration in a file
        outFile.close()
except Exception as ex:
        print 'Something went wrong:'
        print ex
