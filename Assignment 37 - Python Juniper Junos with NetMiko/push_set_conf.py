#!/usr/bin/python3
import netmiko

# Attributes for establishing connection to target devices
myConnection = netmiko.ConnectHandler(ip='10.10.1.1',
                                      device_type='juniper',
                                      username='root',
                                      password='lab123',
                                      verbose=True,
                                      encoding='UTF-8')

myConnection_1 = netmiko.ConnectHandler(ip='10.10.2.1',
                                      device_type='juniper',
                                      username='root',
                                      password='lab123',
                                      verbose=True,
                                      encoding='UTF-8')

def main():
    try:
        # Enter into config_mode
        myConnection.config_mode()
        myConnection_1.config_mode()

        # Specify file path that has to be sent to a junos device
        myConnection.send_config_from_file("/home/osboxes/Desktop/vSRX-1_Set_Commands.conf",
                                                exit_config_mode=False)

        myConnection_1.send_config_from_file("/home/osboxes/Desktop/vSRX-2_Set_Commands.conf",
                                                exit_config_mode=False)

        # Commit the set commands
        myConnection.commit(and_quit=True)
        myConnection_1.commit(and_quit=True)

        # Exit configuration mode
        myConnection.exit_config_mode()
        myConnection_1.exit_config_mode()

        # Try to gracefully close the SSH connection
        myConnection.disconnect()
        myConnection_1.disconnect()

    # Catch and print any exceptions
    except Exception as a:
        print(a)
main()