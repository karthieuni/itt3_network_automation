from jnpr.junos import Device
from pprint import pprint
import sys

HOSTIP = '10.217.19.162'

Device.auto_probe = 3 # Default device to connect, add timeout for device
dev = Device(host = HOSTIP, user = 'root', password = 'lab123') # Authentication
dev.open()
route = (dev.cli("show route", warning=False)) # Disable warnings
print (route.split()) #split - convert string to list
dev.close()
