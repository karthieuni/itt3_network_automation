import paramiko, time, os
try:
        sshClient = paramiko.SSHClient()
        sshClient.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print 'Trying to connect to 10.217.19.160'
        sshClient.connect('10.217.19.160', username='root', password='lab123')

        channel = sshClient.invoke_shell()
        print 'SRX shell invoked'
        time.sleep(1)

        os.system('clear')

        routerOutput = channel.recv(1000) # Read router replies - empty buffer

        config = '/config/juniper.conf.gz'
        configCopy = '//home/osboxes/juniper4.conf.gz'

        channel.send('scp ' + config +' osboxes@10.217.19.164:' + configCopy +'\n')
        time.sleep(1)

        channel.send('osboxes.org\n')
        time.sleep(1)

        output = channel.recv(1000)
        print output
        channel.close()

except Exception as ex:
        print 'Something went wrong.'
        print ex