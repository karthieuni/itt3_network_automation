# Descriptions for all three programs #

* ***retrieve_conf.py*** is a program that “simultaneously” retrieves Junos configuration from two Junos devices using NetMiko. The script stores both SRX configurations as separate files.

* ***create_set_conf.py*** is a program that “simultaneously” retrieves Junos set commands from two Junos devices using NetMiko. The script stores both SRX set commands as separate files. The files can then be accessed and edited (i.e., add or delete interfaces). Changes in the created files will not affect Junos devices.

* ***push_set_conf.py*** is a program that “simultaneously” sends Junos set commands to two Junos devices using NetMiko. The script sends and commits SRX set commands as files created by using create_set_conf.py script to two Junos devices.