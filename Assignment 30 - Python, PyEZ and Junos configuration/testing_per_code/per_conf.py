 This program retrieves a Junos configuration by RPC and the Config class
# The configuration is then saved in text format to a file
# The user can specify a file name for the configuration
# The program halts and the user can then edit the configuration 
# The program then uploads the configuration from the file to Junos
# Per Dahlstroem V01
from jnpr.junos import Device
from lxml import etree # XML Element tree
from jnpr.junos.utils.config import Config

hostIP = '10.217.19.162'
defaultConfigFile = 'myConfigFile.conf' # Default configuration file name
myConfigPath = '/home/junosConfigurations/'

def main():
    # myConfigPath = '/home/junosConfigurations/'
    dev = Device(host = hostIP, user = 'root', password = 'lab123')
    print ('Opening connection to ', hostIP)
    dev.open() # Fetch text configuration:
    myConfig = dev.rpc.get_config(options={'format':'text'})
    myTextConfig = etree.tostring(myConfig, encoding='unicode', \
    pretty_print=True) # Type convert from lxml.etree._Element to string
    # Remove the leading and trailing xml tags from configuration
    myTextConfig = '\n'.join(myTextConfig.split('\n')[1:-2])

    configFile = input('Please enter config file name in this format (**add a name**.conf): ')
    if not configFile: configFile = defaultConfigFile # No name was entered
    myConfigFilePath = myConfigPath + configFile

    myConfigFile = open(myConfigFilePath, 'w')
    myConfigFile.write(myTextConfig) # Write Junos text configuration to file
    myConfigFile.close()

    input('\nPlease EDIT the config file located in: ' + myConfigFilePath + \
          '\nPress ENTER when done:') # Halt program for user to edit configuration

    print('Opening and uploading configuration file: ' + configFile + ' listed here:')
    configInFile = open(myConfigFilePath, 'r')
    fileContent = configInFile.read() # Read configuration in
    configInFile.close()
    print(fileContent) # This is the config that will be sent to Junos

    dev.bind(cu=Config) # Bind the Config instance to the Device instance.
    dev.cu.lock() # Lock the configuration.
    dev.cu.load(path=myConfigFilePath, overwrite=True) # Load configuration to Junos
    dev.cu.commit() # Commit the configuration on Junos device
    dev.cu.unlock() # Unlock the configuration
    dev.close()
    print('The listed config was sucessfully send to the Junos device at ' + hostIP)

main()

